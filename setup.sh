#!/bin/sh

sudo apt-get -y install git zsh
or
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
brew install git zsh

git clone git@gitlab.com:arlininger/dot-files.git ~/dot-files
curl -L https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh | sh
rm -f ~/.zshrc; ln -s ~/dot-files/zshrc ~/.zshrc
rm -f ~/.screenrc; ln -s ~/dot-files/screenrc ~/.screenrc
rm -f ~/.vimrc; ln -s ~/dot-files/vimrc ~/.vimrc
ln -s ~/dot-files/lininger.zsh-theme ~/.oh-my-zsh/themes/lininger.zsh-theme


