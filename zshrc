if [[ -d $HOME/.oh-my-zsh ]] then
    # Uncomment the following line to use case-sensitive completion.
    # CASE_SENSITIVE="true"

    # Uncomment the following line to disable bi-weekly auto-update checks.
    # DISABLE_AUTO_UPDATE="true"

    # Uncomment the following line to change how often to auto-update (in days).
    export UPDATE_ZSH_DAYS=1

    # Uncomment the following line to disable colors in ls.
    # DISABLE_LS_COLORS="true"

    # Uncomment the following line to disable auto-setting terminal title.
    # DISABLE_AUTO_TITLE="true"

    # Uncomment the following line to enable command auto-correction.
    # ENABLE_CORRECTION="true"

    # Uncomment the following line to display red dots whilst waiting for completion.
    COMPLETION_WAITING_DOTS="true"

    # Uncomment the following line if you want to disable marking untracked files
    # under VCS as dirty. This makes repository status check for large repositories
    # much, much faster.
    # DISABLE_UNTRACKED_FILES_DIRTY="true"

    # Uncomment the following line if you want to change the command execution time
    # stamp shown in the history command output.
    # The optional three formats: "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
    # HIST_STAMPS="mm/dd/yyyy"

    # Would you like to use another custom folder than $ZSH/custom?
    # ZSH_CUSTOM=/path/to/new-custom-folder

	# Path to your oh-my-zsh installation.
	export ZSH=$HOME/.oh-my-zsh

	# Set name of the theme to load.
	# Look in ~/.oh-my-zsh/themes/
	# Optionally, if you set this to "random", it'll load a random theme each
	# time that oh-my-zsh is loaded.
	ZSH_THEME="lininger"
	
	# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
	# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
	# Example format: plugins=(rails git textmate ruby lighthouse)
	# Add wisely, as too many plugins slow down shell startup.
	plugins=(git git-extras macports macos pep8)

	source $ZSH/oh-my-zsh.sh
fi

# User configuration

if [[ -d /usr/local/share/zsh-completions ]] then
    fpath=(/usr/local/share/zsh-completions $fpath)
fi

export PATH="$PATH:$HOME/bin"

# You may need to manually set your language environment
# export LANG=en_US.UTF-8

export EDITOR='vim'
export PAGER="less -RFX"
# Preferred editor for local and remote sessions
# if [[ -n $SSH_CONNECTION ]]; then
#   export EDITOR='vim'
# else
#   export EDITOR='mvim'
# fi

# ssh
# export SSH_KEY_PATH="~/.ssh/dsa_id"

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.
#
# Example aliases
# alias zshconfig="mate ~/.zshrc"
# alias ohmyzsh="mate ~/.oh-my-zsh"

setopt interactivecomments

if [[ -f $HOME/.iterm2_shell_integration.zsh ]] then
    source $HOME/.iterm2_shell_integration.zsh
fi

alias vagrant="NOKOGIRI_USE_SYSTEM_LIBRARIES=1 vagrant"

binnify() {
    grep -o -E "[0-9a-fA-F]{8} [0-9a-fA-F]{8} [0-9a-fA-F]{8} [0-9a-fA-F]{8}" $1 | xxd -g 4 -r -plain - $1.bin
}

export HOMEBREW_GITHUB_API_TOKEN="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"
export PATH="/opt/homebrew/bin:/usr/local/sbin:$PATH"

export PATH="$PATH:$HOME/.rvm/bin" # Add RVM to PATH for scripting

# Local Defines
if [[ -f $HOME/.zshrc_local ]] then
	source $HOME/.zshrc_local
fi

