set nocompatible              " be iMproved, required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
try
    call vundle#begin()
    " alternatively, pass a path where Vundle should install plugins
    "call vundle#begin('~/some/path/here')
    " let Vundle manage Vundle, required
    Plugin 'gmarik/Vundle.vim'

    Plugin 'einars/js-beautify'
    Plugin 'JuliaLang/julia-vim'
    " All of your Plugins must be added before the following line
    call vundle#end()            " required
catch /^Vim/
    " Don't really care
endtry
filetype plugin indent on    " required

" To ignore plugin indent changes, instead use:
"filetype plugin on
"
" Brief help
" :PluginList       - lists configured plugins
" :PluginInstall    - installs plugins; append `!` to update or just :PluginUpdate
" :PluginSearch foo - searches for foo; append `!` to refresh local cache
" :PluginClean      - confirms removal of unused plugins; append `!` to auto-approve removal
"
" see :h vundle for more details or wiki for FAQ
" Put your non-Plugin stuff after this line


" automatically indent on new lines to match indentation of the previous line
set autoindent

" ignore case in text searches
set ignorecase

" allows pattern matching with special characters
set magic

" status bar will tell you when you're in insert mode
set showmode

" some stuff to do with making tabs work correctly
set tabstop=4
set shiftwidth=4
set softtabstop=4
set smarttab
set expandtab

" turn on syntax highlighting
syntax on

" show line numbers
set number

" highlight search matches
set hlsearch
" highlight matches while typing search string
set incsearch

" cscope plugin settings
set cspc=3
set csverb
set cst

" makes backspace work correctly
set backspace=indent,eol,start
set nocompatible
set smartindent

autocmd BufNewFile,BufReadPost *.md set filetype=markdown

silent! execute pathogen#infect()

colorscheme desert
set background=dark

source $VIMRUNTIME/mswin.vim
behave mswin

